import Vue from 'vue'
import VueRouter from 'vue-router'
import TaskList from './components/TaskList.vue'
import Task from './components/Task.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/Tasks'
  },
  {
    path: '/Tasks',
    name: 'tasklist',
    component: TaskList
  },
  {
    path: '/Tasks/:id',
    name: 'task',
    component: Task,
    props: true
  },
]
 
export default new VueRouter({
  routes: routes,
  mode: 'history'
})
